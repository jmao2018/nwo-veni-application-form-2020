# NWO Veni application form 2020

The aim is to reproduce as much as possible the original Microsoft Word 
application form (science domain) that is available from the 
[NWO Talent Programme Veni - Science Domain](https://www.nwo.nl/en/funding/our-funding-instruments/nwo/innovational-research-incentives-scheme/veni/ew/innovational-research-incentives-scheme-veni-enw.html). Please modify relevant sections if the proposal is submitted to other domains.  

In order to use this template on [Overleaf](https://www.overleaf.com), please set the compiler to lualatex. 

To compile the document run:
```
lualatex veni-template.tex
```
